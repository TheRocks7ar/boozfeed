//
//  Fixtures.swift
//  BoozFeedTests
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

struct Fixtures {
  enum JSON {
    case beers
		case ingredients
		case foodPairings
		case malt
		case hops
  }
	
	static let description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum."
}

extension Fixtures.JSON {
	
  var anyValue: Any {
    switch self {
    case .beers:
      return
			[
				["name": "Awesome Beer 0", "image_url": "0", "abv": 1.0, "first_brewed": "01/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 1", "image_url": "1", "abv": 1.1, "first_brewed": "02/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 2", "image_url": "2", "abv": 1.2, "first_brewed": "03/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 3", "image_url": "3", "abv": 1.3, "first_brewed": "04/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 4", "image_url": "4", "abv": 1.4, "first_brewed": "05/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 5", "image_url": "5", "abv": 1.5, "first_brewed": "06/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 6", "image_url": "6", "abv": 1.6, "first_brewed": "07/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 7", "image_url": "7", "abv": 1.7, "first_brewed": "08/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 8", "image_url": "8", "abv": 1.8, "first_brewed": "09/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ],
				[ "name": "Awesome Beer 9", "image_url": "9", "abv": 1.9, "first_brewed": "10/1988", "description": Fixtures.description, "ingredients": Fixtures.JSON.ingredients.anyValue, "food_pairing": Fixtures.JSON.foodPairings.anyValue ]
			]
		case .ingredients:
			return [ "malt": Fixtures.JSON.malt.anyValue, "hops": Fixtures.JSON.hops.anyValue ]
		case .foodPairings:
			return [ "with chicken", "with deep fried kangeroo", "honey and cereal, dinosaur meat", "carrots and kidney beans" ]
		case .malt:
			return
					[
						[ "name": "Awesome Malt 0", "amount": [ "value": 1.0, "unit": "kilograms" ] ],
						[ "name": "Awesome Malt 1", "amount": [ "value": 1.1, "unit": "kilograms" ] ],
						[ "name": "Awesome Malt 2", "amount": [ "value": 1.2, "unit": "kilograms" ] ],
						[ "name": "Awesome Malt 3", "amount": [ "value": 1.3, "unit": "kilograms" ] ],
						[ "name": "Awesome Malt 4", "amount": [ "value": 1.4, "unit": "kilograms" ] ]
					]
		case .hops:
			return
					[
						 [ "name": "Awesome Hop 0", "amount": [ "value": 1.0, "unit": "grams" ], "add": "start", "attribute": "bitter" ],
						 [ "name": "Awesome Hop 1", "amount": [ "value": 1.1, "unit": "grams" ], "add": "middle", "attribute": "flavour" ],
						 [ "name": "Awesome Hop 2", "amount": [ "value": 1.2, "unit": "grams" ], "add": "end", "attribute": "bitter" ],
						 [ "name": "Awesome Hop 3", "amount": [ "value": 1.3, "unit": "grams" ], "add": "start", "attribute": "flavour" ],
						 [ "name": "Awesome Hop 4", "amount": [ "value": 1.4, "unit": "grams" ], "add": "end", "attribute": "bitter" ]
					]
			}
		}
	var jsonData: Data {
		return try! JSONSerialization.data(withJSONObject: anyValue, options: .prettyPrinted)
	}
}
