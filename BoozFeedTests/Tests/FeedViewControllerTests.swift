//
//  FeedViewControllerTests.swift
//  
//
//  Created by Sayeem Hussain on 10/29/19.
//

import XCTest
@testable import BoozFeed

class FeedViewControllerTests: XCTestCase {
	
	var sut: FeedViewController!
	let injectionContainer = BoozFeedAppDependencyContainer()
	
	override func setUp() {
		super.setUp()
		sut = injectionContainer.makeBeerFeedViewController()
	}

	override func tearDown() {
		super.tearDown()
		sut = nil
	}

//	func test_tableView() {
//		_ = sut.view
		
//		assert(sut.rootView.tableView.visibleCells.isEmpty == false)
//	}
	
	func test_viewModel_binding_beerFeedHandler() {
		_ = sut.view
		
		XCTAssertNotNil(sut.viewModel.beerFeedHandler)
	}
	
	func test_viewModel_binding_selectedbeerHandler() {
		_ = sut.view
		
		XCTAssertNotNil(sut.viewModel.selectedbeerHandler)
	}
}
