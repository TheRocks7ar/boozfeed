//
//  FeedViewModelTests.swift
//  BoozFeedTests
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import XCTest
@testable import BoozFeed

class FeedViewModelTests: XCTestCase {
	
	let injectionContainer = BoozFeedAppDependencyContainer()
	var sut: FeedViewModel!
	
	override func setUp() {
		super.setUp()
		sut = injectionContainer.makeFeedViewModel()
	}

	override func tearDown() {
		super.tearDown()
		sut = nil
	}

	func test_beerFeedHandler_trigger() {
		let triggerExpectation = expectation(description: "beerFeedHandler does trigger")
		sut.beerFeedHandler = { beerViewModels in
			triggerExpectation.fulfill()
		}
		wait(for: [triggerExpectation], timeout: 0)
	}
	
	func test_beerhandler_viewModels() {
		sut.beerFeedHandler = { beerViewModels in
			assert(beerViewModels.count == 10)
			assert(beerViewModels.first!.name == "Awesome Beer 0")
			assert(beerViewModels.last!.name == "Awesome Beer 9")
		}
	}
	
	func test_beerFeedHandler_viewModel_trigger() {
		sut.beerViewModels = []
		sut.beerFeedHandler = { beerViewModels in
			assert(beerViewModels.isEmpty)
		}
	}
	
	func test_selectedbeerHandler_trigger() {
		let triggerExpectation = expectation(description: "selectedBeerHandler does trigger")
		sut.selectedbeerHandler = { beerViewModel in
			triggerExpectation.fulfill()
		}
		sut.selectedBeer(indexPath: IndexPath(row: 0, section: 0))
		wait(for: [triggerExpectation], timeout: 0)
	}
	
	func test_beerFeedHandler_viewModel() {
		sut.selectedbeerHandler = { beerViewModel in
			assert(beerViewModel.name == "Awesome Beer 0")
		}
		sut.selectedBeer(indexPath: IndexPath(row: 0, section: 0))
	}
	
	func test_mutable_page_property() {
		for _ in 0..<5 {
			sut.nextPage()
		}
		assert(sut.page == 7)
	}
	
	func test_paging_viewModel_count() {
		for _ in 0..<5 {
			sut.nextPage()
		}
		sut.beerFeedHandler = { beerViewModels in
			assert(self.sut.page * 10 == 70)
			assert(beerViewModels.count == 60)
		}
	}
}
