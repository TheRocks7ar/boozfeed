//
//  HttpClientTests.swift
//  BoozFeedTests
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import XCTest
@testable import BoozFeed

// MARK: Test
class HttpClientTests: XCTestCase {
	  
	var sut: HttpClient!
	let session = FakeURLSession()
	
	override func setUp() {
		super.setUp()
		sut = HttpClient(session: session)
	}
	
	override func tearDown() {
		super.tearDown()
		sut = nil
	}
	
	func test_get_request_with_URL() {
		guard let url = URL(string: "https://mockurl") else {
				fatalError("URL can't be empty")
		}
		
		sut.get(url: url) { (_, _) in
				// Return data
		}
		
		XCTAssert(session.lastURL == url)
	}
	
	func test_get_resume_called() {
		let dataTask = MockURLSessionDataTask()
		session.nextDataTask = dataTask
		
		guard let url = URL(string: "https://mockurl") else {
				fatalError("URL can't be empty")
		}
		
		sut.get(url: url) { (_, _) in
				// Return data
		}
		
		XCTAssert(dataTask.resumeWasCalled)
	}
	
	func test_get_should_return_data() {
			let expectedData = "{}".data(using: .utf8)
			
			session.nextData = expectedData
			
			let exp = expectation(description: "should recieve data")
			var actualData: Data?
			sut.get(url: URL(string: "https://mockurl")!) { (data, _) in
					actualData = data
					XCTAssertNotNil(actualData)
					exp.fulfill()
			}
			wait(for: [exp], timeout: 3)
	}
}
