//
//  HopViewModel.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class HopViewModel {
	
	// MARK: - Properties
	let hop: Hop
	
	var name: String {
		hop.name
	}
	var amount: String {
		return ("\(hop.amount.value) \(hop.amount.unit.rawValue)")
	}
	var add: Hop.Add {
		return hop.add
	}
	var attribute: Hop.Attribute {
		return hop.attribute
	}
	
	// MARK: - Methods
	init(hop: Hop) {
		self.hop = hop
	}
	
	func hopModel() -> Hop {
		return hop
	}
}
