//
//  IngredientsViewModel.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class IngredientsViewModel {
	
	// MARK: - Properties
	let ingredients: Ingredients
	let malt: [MaltViewModel]
	let hops: [HopViewModel]
	
	// MARK: - Methods
	init(ingredients: Ingredients) {
		self.ingredients = ingredients
		self.malt = ingredients.malt.map { MaltViewModel(malt: $0) }
		self.hops = ingredients.hops.map { HopViewModel(hop: $0) }
	}
	
	func ingredientsModel() -> Ingredients {
		return ingredients
	}
}
