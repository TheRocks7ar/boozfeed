//
//  FeedViewModel.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class FeedViewModel {
	
	// MARK: - Properties
	private let repository: Repository
	public var page: Int
	private var isFetching: Bool
	
	public var beerViewModels: [BeerViewModel] = [] {
		didSet {
			beerFeedHandler?(beerViewModels)
		}
	}
	public var beerFeedHandler: (([BeerViewModel]) -> Void)? {
		didSet {
			beerFeedHandler?(beerViewModels)
		}
	}
	public var beerFeedFailureHandler: ((String) -> Void)?
	public var selectedbeerHandler: ((BeerViewModel) -> Void)?
	
	// MARK: - Methods
	public init(repository: Repository) {
		self.repository = repository
		self.page = 1
		self.isFetching = false
		
		retreiveBeers()
	}
	
	private func retreiveBeers() {
		isFetching.toggle()
		repository.retreiveBeers(page: page) { result in
			self.isFetching.toggle()
			switch result {
			case .success(let beers):
				self.page += 1
				self.beerViewModels += beers.map { BeerViewModel(beer: $0) }
			case .failure(let error):
				self.beerFeedFailureHandler?(error.message)
			case .none: break
			}
		}
	}
	
	public func selectedBeer(indexPath: IndexPath) {
		let beerViewModel = beerViewModels[indexPath.row]
		selectedbeerHandler?(beerViewModel)
	}
	
	public func nextPage() {
		guard !isFetching else { return }
		retreiveBeers()
	}
}
