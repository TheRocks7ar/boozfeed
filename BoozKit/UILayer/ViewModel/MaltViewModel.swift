//
//  MaltViewModel.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class MaltViewModel {
	
	// MARK: - Properties
	let malt: Malt
	
	var name: String {
		malt.name
	}
	var amount: String {
		return ("\(malt.amount.value) \(malt.amount.unit.rawValue)")
	}
	
	// MARK: - Methods
	init(malt: Malt) {
		self.malt = malt
	}
	
	func maltModel() -> Malt {
		return malt
	}
}
