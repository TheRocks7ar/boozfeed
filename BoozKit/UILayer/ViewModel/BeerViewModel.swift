//
//  BeerViewModel.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class BeerViewModel {
	
	// MARK: - Properties
	let beer: Beer
	
	var name: String {
		return beer.name
	}
	var imageURLString: String {
		return beer.imageURLString
	}
	var abv: Float {
		return beer.abv
	}
	var description: String {
		return beer.description
	}
	var firstBrewed: String {
		return beer.firstBrewed
	}
	var foodParings: [String] {
		return beer.foodPairings
	}
	var ingredients: Ingredients {
		return beer.ingredients
	}
	
	// MARK: - Methods
	init(beer: Beer) {
		self.beer = beer
	}
	
	func beerModel() -> Beer {
		return beer
	}
}
