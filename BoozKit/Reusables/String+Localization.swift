//
//  String+Localization.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

private func NSLocalizedString(_ key: String) -> String {
    return NSLocalizedString(key, comment: "")
}

extension String {
	
	// MARK: - Properties
	static let Ingredients = NSLocalizedString("Ingredients")
	static let Details = NSLocalizedString("Details")
	static let BoozFeed = NSLocalizedString("BoozFeed")
	static let Retry = NSLocalizedString("Retry")
	static let Malt = NSLocalizedString("Malt")
	static let Hops = NSLocalizedString("Hops")
	
	// MARK: - Methods
	var localized: String {
		return NSLocalizedString(self)
	}
}
