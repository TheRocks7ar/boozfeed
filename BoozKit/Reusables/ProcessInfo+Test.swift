//
//  ProcessInfo+Test.swift
//  BoozKit
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

extension ProcessInfo {
	
	// MARK: - Properties
	class var isTesting: Bool {
		processInfo.arguments.contains("TEST")
	}
}
