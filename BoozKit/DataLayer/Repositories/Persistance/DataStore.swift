//
//  DataStore.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public protocol DataStore {
	
	func loadFavouriteBeers(completion: @escaping (Result<Beer>) -> Void)
}
