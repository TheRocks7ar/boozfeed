//
//  BeerFeedRepository.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public protocol Repository {
	
	func retreiveBeers(page: Int, completion: @escaping (Result<[Beer]>) -> Void)
	func retrieveBeer(id: Double, completion: @escaping (Result<Beer>) -> Void)
	func loadFavouriteBeers(completion: @escaping (Result<Beer>) -> Void)
}
