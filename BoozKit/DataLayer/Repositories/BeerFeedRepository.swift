//
//  BeerFeedRepository.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class BeerFeedRepository: Repository {
	
	// MARK: - Properties
	let remoteAPI: RemoteAPI
	let dataStore: DataStore
	
	// MARK: - Methods
	init(remoteAPI: RemoteAPI, dataStore: DataStore) {
		self.remoteAPI = remoteAPI
		self.dataStore = dataStore
	}
	
	public func retreiveBeers(page: Int, completion: @escaping (Result<[Beer]>) -> Void) {
		remoteAPI.retreiveBeers(page: page) { result in
			completion(result)
		}
	}
	
	public func retrieveBeer(id: Double, completion: @escaping (Result<Beer>) -> Void) {
		remoteAPI.retreiveBeer(id: id) { result in
			completion(result)
		}
	}
	
	public func loadFavouriteBeers(completion: @escaping (Result<Beer>) -> Void) {
		dataStore.loadFavouriteBeers { result in
			completion(result)
		}
	}
}
