//
//  RequestError.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public struct RequestError: Error {
	// MARK: - Properties
  let message: String
  let code: Int?
	
	// MARK: - Methods
  init(message: String) {
    self.message = message
    self.code = nil
  }

  init(message: String, errorCode: Int? = nil) {
    self.message = message
    self.code = errorCode
  }
}
