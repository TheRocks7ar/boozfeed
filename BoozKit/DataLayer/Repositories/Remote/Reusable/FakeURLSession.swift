//
//  FakeURLSession.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class FakeURLSession: URLSessionProtocol {
	
	// MARK: - Properties
	var nextDataTask = MockURLSessionDataTask()
	var nextData: Data?
	var nextError: Error?
	
	private(set) var lastURL: URL?
	
	// MARK: - Methods
	public func successHttpURLResponse(request: URLRequest) -> URLResponse {
		return HTTPURLResponse(url: request.url!, statusCode: 200, httpVersion: "HTTPS/1.1", headerFields: nil)!
	}
	
	public func dataTask(with request: URLRequest, completionHandler: @escaping DataTaskResult) -> URLSessionDataTaskProtocol {
		lastURL = request.url
		
		completionHandler(nextData, successHttpURLResponse(request: request), nextError)
		return nextDataTask
	}
}

public class MockURLSessionDataTask: URLSessionDataTaskProtocol {
	
	// MARK: - Properties
	private(set) var resumeWasCalled = false
  
	// MARK: - Methods
	public func resume() {
		resumeWasCalled = true
	}
}
