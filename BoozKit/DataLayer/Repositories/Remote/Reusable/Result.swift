//
//  Result.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public enum Result<T> {
  case success(T)
  case none
  case failure(error: RequestError)
}
