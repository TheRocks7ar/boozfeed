//
//  HTTPClient.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class HttpClient {
	
    // MARK: - Properties
    public typealias completeClosure = ( _ data: Data?, _ rrror: RequestError?) -> Void
    
    private let session: URLSessionProtocol
    
	// MARK: - Methods
    public init(session: URLSessionProtocol) {
        self.session = session
    }
    
    public func get( url: URL, completion: @escaping completeClosure ) {
        var request = URLRequest(url: url)
        request.httpMethod = "GET"
        let task = session.dataTask(with: request) { (data, response, error) in
					if error != nil {
						let error = RequestError(message: NetworkResponse.noNetwork.rawValue, errorCode: nil)
						DispatchQueue.main.async {
							completion(nil, error)
						}
						return
					}
					if let response = response as? HTTPURLResponse {
						let result = ResponseHandler.handleNetworkResponse(response)
						switch result {
						case .success:
							guard let responseData = data else {
								let error = RequestError(message: NetworkResponse.noData.rawValue, errorCode: nil)
								DispatchQueue.main.async {
									completion(nil, error)
								}
								return
							}
							DispatchQueue.main.async {
								completion(responseData, nil)
							}
						case .failure(let error):
							DispatchQueue.main.async {
								completion(nil, error)
							}
						default: break
						}
					} else {
						let error = RequestError(message: NetworkResponse.failed.rawValue, errorCode: nil)
						DispatchQueue.main.async {
							completion(nil, error)
						}
					}
        }
        task.resume()
    }
}
