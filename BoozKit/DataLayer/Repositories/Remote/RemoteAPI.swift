//
//  RemoteAPI.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

protocol RemoteAPI {
	
	func retreiveBeers(page: Int, completion: @escaping (Result<[Beer]>) -> Void)
	func retreiveBeer(id: Double, completion: @escaping (Result<Beer>) -> Void)
}
