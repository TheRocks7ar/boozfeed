//
//  FakeBeerFeedAPI.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation
//swiftlint:disable force_try

public class FakeBeerFeedAPI: RemoteAPI {
	
	// MARK: - Methods
	public func retreiveBeers(page: Int, completion: @escaping (Result<[Beer]>) -> Void) {
		let beerData = Fixtures.JSON.beers.jsonData
		let beers = try! JSONDecoder().decode([Beer].self, from: beerData)
		completion(.success(beers))
	}
	
	public func retreiveBeer(id: Double, completion: @escaping (Result<Beer>) -> Void) {
	}
}
