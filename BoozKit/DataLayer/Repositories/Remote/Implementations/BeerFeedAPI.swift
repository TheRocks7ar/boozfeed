//
//  BeerFeedAPI.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public class BeerFeedAPI: RemoteAPI {
	
	// MARK: - Methods
	public func retreiveBeers(page: Int, completion: @escaping (Result<[Beer]>) -> Void) {
		let session = URLSession(configuration: .ephemeral)
		let client = HttpClient(session: session)
		
		let queryParam = "page=\(page)&per_page=10"
		guard let url = URL(string: "https://api.punkapi.com/v2/beers?\(queryParam)") else {
			return
		}
		client.get(url: url) { (data, error) in
			guard let dataResonse = data else {
				completion(.failure(error: error ?? RequestError(message: NetworkResponse.noNetwork.rawValue)))
				return
			}
			do {
				let models = try JSONDecoder().decode([Beer].self, from: dataResonse)
				completion(.success(models))
			} catch {
				let requestError = RequestError(message: NetworkResponse.unableToDecode.rawValue)
				completion(.failure(error: requestError))
			}
		}
	}
	
	public func retreiveBeer(id: Double, completion: @escaping (Result<Beer>) -> Void) {
	}
}
