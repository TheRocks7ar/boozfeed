//
//  Malt.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public struct Malt: Decodable {
	
	// MARK: - Properties
	public let name: String
	public let amount: Amount
}
