//
//  Ingredients.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public struct Ingredients: Decodable {
	
	// MARK: - Properties
	public let malt: [Malt]
	public let hops: [Hop]
}
