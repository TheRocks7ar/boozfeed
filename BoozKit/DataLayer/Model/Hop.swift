//
//  Hop.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public struct Hop: Decodable {
	
	enum CodingKeys: String, CodingKey {
		case name
		case amount
		case add
		case attribute
  }
	
	public enum Add: String, Decodable {
		case start, middle, end
	}
	
	public enum Attribute: String, Decodable {
		case bitter, flavour
	}
	
	// MARK: - Properties
	public let name: String
	public let amount: Amount
	public let add: Add
	public let attribute: Attribute
	
	// MARK: - Methods
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		let addValue = try container.decode(String.self, forKey: .add)
		let attributeValue = try container.decode(String.self, forKey: .attribute)
		
		self.name = try container.decode(String.self, forKey: .name)
		self.amount = try container.decode(Amount.self, forKey: .amount)
		self.add = Add(rawValue: addValue) ?? .start
		self.attribute = Attribute(rawValue: attributeValue) ?? .bitter
	}
}
