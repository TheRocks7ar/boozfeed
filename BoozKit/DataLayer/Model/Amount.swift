//
//  Amount.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public struct Amount: Decodable {
	
	enum CodingKeys: String, CodingKey {
		case value
		case unit
  }
	
	public enum Unit: String, CodingKey {
		case grams, kilograms
	}
	
	// MARK: - Properties
	public let value: Float
	public let unit: Unit
	
	// MARK: - Methods
	public init(from decoder: Decoder) throws {
		let container = try decoder.container(keyedBy: CodingKeys.self)
		
		let unitValue = try container.decode(String.self, forKey: .unit)
		
		self.value = try container.decode(Float.self, forKey: .value)
		self.unit = Unit(rawValue: unitValue) ?? .grams
	}
}
