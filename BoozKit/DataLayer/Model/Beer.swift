//
//  Beer.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

public struct Beer: Decodable {
	
  enum CodingKeys: String, CodingKey {
    case name
    case imageURLString = "image_url"
		case abv
		case firstBrewed = "first_brewed"
		case description
		case ingredients
		case foodPairings = "food_pairing"
  }
	
	// MARK: - Properties
	public let name: String
	public let imageURLString: String
	public let abv: Float
	public let firstBrewed: String
	public let description: String
	public let ingredients: Ingredients
	public let foodPairings: [String]
}
