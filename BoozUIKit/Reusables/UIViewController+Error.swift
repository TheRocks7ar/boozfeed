//
//  UIViewController+Error.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

extension UIViewController {
	
	// MARK: - Methods
  public func alert(title: String = NSLocalizedString("Something went wrong", comment: ""), message: String, action: String = "Close", handler: ((UIAlertAction) -> Void)? = nil) {
    let alert = UIAlertController(title: title,
                                  message: message,
                                  preferredStyle: .alert)
    alert.addAction(UIAlertAction(title: NSLocalizedString(action, comment: ""),
                                  style: .cancel, handler: handler))
    present(alert, animated: true, completion: nil)
	}
}
