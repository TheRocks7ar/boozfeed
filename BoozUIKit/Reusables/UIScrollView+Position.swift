//
//  UIScrollView+Position.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

extension UIScrollView {
	
	// MARK: - Properties
	var maxLeniency: CGFloat { return frame.height * 0.1 }
	
	var isCurrentlyScrolling: Bool {
		 return isDragging || isDecelerating
	}
  
	// MARK: - Methods
	func isScrollViewAtBottom() -> Bool {
    return contentInset.top + contentOffset.y + maxLeniency > contentSize.height - bounds.height
  }
}
