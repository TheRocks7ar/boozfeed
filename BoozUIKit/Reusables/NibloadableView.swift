//
//  NibloadableView.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit
//swiftlint:disable force_cast

public protocol NibLoadableView: class { }

extension NibLoadableView where Self: UIView {
  
	// MARK: - Properties
  static var NibName: String {
    return String(describing: self)
  }
  
	// MARK: - Methods
  static func loadFromNib() -> Self {
    let nib = UINib(nibName: Self.NibName, bundle: nil)
    return nib.instantiate(withOwner: nil, options: nil).first as! Self
  }
}

extension NibLoadableView where Self: UIViewController {
	
	// MARK: - Methods
	static func loadFromNib() -> Self {
    func instantiateFromNib<T: UIViewController>() -> T {
      return T(nibName: String(describing: T.self), bundle: nil)
    }
    
    return instantiateFromNib()
  }
}
