//
//  UITableView+Cell.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit
//swiftlint:disable force_cast

extension UITableView {
	
	// MARK: - Methods
  public func dequeueReusableCell<T: UITableViewCell>(_ cell: T.Type) -> T where T: ReusableCell {
    return dequeueReusableCell(withIdentifier: cell.reusableIdentifier) as! T
  }
	
  public func register<T: UITableViewCell>(_: T.Type) where T: ReusableCell, T: NibLoadableView {
    let nib = UINib(nibName: T.NibName, bundle: nil)
    register(nib, forCellReuseIdentifier: T.reusableIdentifier)
  }
}
