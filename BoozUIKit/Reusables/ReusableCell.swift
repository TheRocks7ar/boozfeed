//
//  ReusableCell.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

public protocol ReusableCell: class {}

extension ReusableCell where Self: UITableViewCell {
	
	// MARK: - Properties
  static var reusableIdentifier: String { return String(describing: self) }
}
