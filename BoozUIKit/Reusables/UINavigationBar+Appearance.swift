//
//  UINavigationBar+Appearance.swift
//  BoozUIKit
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

extension UINavigationBar {
	
	// MARK: - Methods
	class func styleGlobalAppearance() {
    appearance().shadowImage = UIImage()
    appearance().isTranslucent = true
    appearance().barTintColor = UIColor.white
    appearance().titleTextAttributes =  [NSAttributedString.Key.font: UIFont.systemFont(ofSize: 16)]
	}
}
