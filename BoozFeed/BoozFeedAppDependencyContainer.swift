//
//  BoozFeedAppDependencyContainer.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/29/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import Foundation

class BoozFeedAppDependencyContainer {
	
	// MARK: - Methods
	// Beer Feed
	public func makeBeerFeedViewController() -> FeedViewController {
		let beerDetailsViewControllerFactory = { beerViewModel in
			return self.makeBeerDetailsViewController(beerViewModel: beerViewModel)
		}
		return FeedViewController(feedViewModel: makeFeedViewModel(), beerDetailsViewControllerFactory: beerDetailsViewControllerFactory)
	}
	
	public func makeFeedViewModel() -> FeedViewModel {
		return FeedViewModel(repository: makeBeerFeedRepository())
	}
	
	public func makeBeerFeedRepository() -> BeerFeedRepository {
		return BeerFeedRepository(remoteAPI: makeRemoteAPI(), dataStore: makeDataStore())
	}
	
	public func makeRemoteAPI() -> RemoteAPI {
		if ProcessInfo.isTesting {
			return FakeBeerFeedAPI()
		} else {
			return BeerFeedAPI()
		}
	}
	
	public func makeDataStore() -> DataStore {
		if ProcessInfo.isTesting {
			return FakeBeerFeedDataStore()
		} else {
			return BeerFeedDataStore()
		}
	}
	
	// Beer Details
	public func makeBeerDetailsViewController(beerViewModel: BeerViewModel) -> BeerDetailsViewController {
		let ingredientsViewControllerFactory = { ingredients in
			return self.makeIngredientsViewController(ingredients: ingredients)
		}
		return BeerDetailsViewController(beerViewModel: beerViewModel, ingredientsViewControllerFactory: ingredientsViewControllerFactory)
	}
	
	// Beer Ingredients
	public func makeIngredientsViewController(ingredients: Ingredients) -> IngredientsViewController {
		return IngredientsViewController(ingredientsViewModel: makeIngredientsViewModel(ingredients: ingredients))
	}
	
	public func makeIngredientsViewModel(ingredients: Ingredients) -> IngredientsViewModel {
		return IngredientsViewModel(ingredients: ingredients)
	}
}
