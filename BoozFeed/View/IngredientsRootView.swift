//
//  IngredientsRootView.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

class IngredientsRootView: UIView {
	
	enum Section: Int {
		case malt, hops
	}
	
	// MARK: - Properties
	var ingredientsViewModel: IngredientsViewModel! {
		didSet {
			tableView.tableFooterView = UIView()
		}
	}
	
	@IBOutlet weak private var tableView: UITableView! {
		didSet {
			tableView.register(MaltTableViewCell.self)
			tableView.register(HopTableViewCell.self)
		}
	}
}

// MARK: - UITableViewDataSource
extension IngredientsRootView: UITableViewDataSource {
	
	func numberOfSections(in tableView: UITableView) -> Int {
		if !ingredientsViewModel.malt.isEmpty && !ingredientsViewModel.hops.isEmpty {
			return 2
		} else if !ingredientsViewModel.malt.isEmpty || !ingredientsViewModel.hops.isEmpty {
			return 1
		} else {
			return 0
		}
	}
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		switch section {
		case Section.malt.rawValue:
			return ingredientsViewModel.malt.count
		case Section.hops.rawValue:
			return ingredientsViewModel.hops.count
		default: return 0
		}
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		switch indexPath.section {
		case Section.malt.rawValue:
			let maltViewModel = ingredientsViewModel.malt[indexPath.row]
			let cell = tableView.dequeueReusableCell(MaltTableViewCell.self)
			cell.setup(maltViewModel: maltViewModel)
			return cell
		case Section.hops.rawValue:
			let hopViewModel = ingredientsViewModel.hops[indexPath.row]
			let cell = tableView.dequeueReusableCell(HopTableViewCell.self)
			cell.setup(hopViewModel: hopViewModel)
			return cell
		default: return UITableViewCell()
		}
	}
}

// MARK: - UITableViewDelegate
extension IngredientsRootView: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
		switch section {
		case Section.malt.rawValue:
			return String.Malt
		case Section.hops.rawValue:
			return String.Hops
		default: return nil
		}
	}
}

// MARK: - NibLoadableView
extension IngredientsRootView: NibLoadableView {}
