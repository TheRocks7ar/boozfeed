//
//  IngredientsViewController.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

class IngredientsViewController: NiblessViewController {
	
	// MARK: - Properties
	// View Models
	let viewModel: IngredientsViewModel
	
	// Views
	let rootView: IngredientsRootView
	
	// MARK: - Methods
	public init(ingredientsViewModel: IngredientsViewModel) {
		self.viewModel = ingredientsViewModel
		let rootView = IngredientsRootView.loadFromNib()
		rootView.ingredientsViewModel = ingredientsViewModel
		self.rootView = rootView
		
		super.init()
	}
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		title = String.Ingredients
	}
	
	public override func loadView() {
		self.view = rootView
	}
}
