//
//  BeerDetailsViewController.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

class BeerDetailsViewController: NiblessViewController {
	
	// MARK: - Properties
	// View Models
	let viewModel: BeerViewModel
	// Views
	let rootView: BeerDetailsRootView
	
	// Factories
	let makeIngredientsViewController: (Ingredients) -> IngredientsViewController
	  
	// MARK: - Methods
	public init(beerViewModel: BeerViewModel, ingredientsViewControllerFactory: @escaping (Ingredients) -> IngredientsViewController) {
		self.viewModel = beerViewModel
		let rootView = BeerDetailsRootView.loadFromNib()
		rootView.setup(beerViewModel: viewModel)
		self.rootView = rootView
		self.makeIngredientsViewController = ingredientsViewControllerFactory
		
		super.init()
	}

	public override func viewDidLoad() {
		super.viewDidLoad()
		
		navigationItem.rightBarButtonItem = UIBarButtonItem(title: String.Ingredients, style: .plain, target: self, action: #selector(presentIngredientsView))
		title = String.Details
	}
	
	public override func loadView() {
		self.view = rootView
	}
	
	@objc private func presentIngredientsView() {
		let vc = makeIngredientsViewController(viewModel.ingredients)
		let nav = UINavigationController(rootViewController: vc)
		present(nav, animated: true, completion: nil)
	}
}
