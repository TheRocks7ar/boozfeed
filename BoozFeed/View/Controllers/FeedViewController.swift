//
//  FeedViewController.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

class FeedViewController: NiblessViewController {
	
	// MARK: - Properties
	// View Models
	let viewModel: FeedViewModel
	
	// Views
	let rootView: BeerFeedRootView
	
	// Factories
	let makeBeerDetailsViewController: (BeerViewModel) -> BeerDetailsViewController
	
	// MARK: - Methods
	public init(feedViewModel: FeedViewModel, beerDetailsViewControllerFactory: @escaping (BeerViewModel) -> BeerDetailsViewController) {
		self.viewModel = feedViewModel
		self.rootView = BeerFeedRootView.loadFromNib()
		self.rootView.viewModel = viewModel
		self.makeBeerDetailsViewController = beerDetailsViewControllerFactory
		
		super.init()
	}
	
	public override func viewDidLoad() {
		super.viewDidLoad()
		bindViewModel()
		title = String.BoozFeed
	}
	
	public override func loadView() {
		view = rootView
	}
	
	private func bindViewModel() {
		viewModel.beerFeedHandler = { [weak self] beerViewModels in
			self?.rootView.reload()
		}
		viewModel.selectedbeerHandler = { [weak self] beerViewModel in
			guard let strongSelf = self else { return }
			let vc = strongSelf.makeBeerDetailsViewController(beerViewModel)
			strongSelf.navigationController?.pushViewController(vc, animated: true)
		}
		viewModel.beerFeedFailureHandler = { [weak self] message in
			self?.retry(message: message)
		}
	}
	
  private func retry(message: String) {
		alert(message: message, action: String.Retry) { _ in
			self.viewModel.nextPage()
		}
  }
}
