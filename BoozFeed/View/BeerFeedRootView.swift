//
//  BeerFeedRootView.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

class BeerFeedRootView: UIView {
	
	// MARK: - Properties
	var viewModel: FeedViewModel!
	
	@IBOutlet weak private var tableView: UITableView! {
		didSet {
			tableView.register(BeerFeedTableViewCell.self)
		}
	}
	
	// MARK: - Methods
	public func reload() {
		tableView.reloadData()
	}
}

// MARK: - UIScrollViewDelegate
extension BeerFeedRootView: UIScrollViewDelegate {
	
  func scrollViewDidScroll(_ scrollView: UIScrollView) {
		let shouldFetch = tableView.isScrollViewAtBottom() && tableView.isCurrentlyScrolling
    if shouldFetch {
			viewModel.nextPage()
		}
  }
}

// MARK: - UITableViewDataSource
extension BeerFeedRootView: UITableViewDataSource {
	
	func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
		viewModel.beerViewModels.count
	}
	
	func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
		let beerViewModel = viewModel.beerViewModels[indexPath.row]
		let cell = tableView.dequeueReusableCell(BeerFeedTableViewCell.self)
		cell.setup(beerViewModel: beerViewModel)
		
		return cell
	}
}

// MARK: - UITableViewDelegate
extension BeerFeedRootView: UITableViewDelegate {
	
	func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
		viewModel.selectedBeer(indexPath: indexPath)
	}
}

// MARK: - NibLoadableView
extension BeerFeedRootView: NibLoadableView {}
