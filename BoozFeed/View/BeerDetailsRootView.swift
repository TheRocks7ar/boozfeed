//
//  BeerDetailsRootView.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit
import SDWebImage

class BeerDetailsRootView: UIView {
	
	// MARK: - Properties
	@IBOutlet weak private var beerImageView: UIImageView!
	@IBOutlet weak private var detailesContainer: UIStackView!
	@IBOutlet weak private var nameLabel: UILabel!
	@IBOutlet weak private var abvBrewedLabel: UILabel!
	@IBOutlet weak private var descriptionLabel: UILabel!
	@IBOutlet weak private var foodPairingsTitleLabel: UILabel!
	
	// MARK: - Methods
	public func setup(beerViewModel: BeerViewModel) {
		beerImageView.sd_setImage(with: URL(string: beerViewModel.imageURLString))
		nameLabel.text = beerViewModel.name
		abvBrewedLabel.text = "abv \(beerViewModel.abv) • \(beerViewModel.firstBrewed)"
		descriptionLabel.text = beerViewModel.description
		if beerViewModel.foodParings.isEmpty {
			detailesContainer.removeArrangedSubview(foodPairingsTitleLabel)
		} else {
			beerViewModel.foodParings.forEach {
				let foodPairingLabel = UILabel()
				foodPairingLabel.text = " • \($0)"
				foodPairingLabel.font = UIFont.systemFont(ofSize: 15)
				detailesContainer.addArrangedSubview(foodPairingLabel)
			}
		}
	}
}

// MARK: - NibLoadableView
extension BeerDetailsRootView: NibLoadableView {}
