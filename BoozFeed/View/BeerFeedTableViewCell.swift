//
//  BeerFeedTableViewCell.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit
import SDWebImage

class BeerFeedTableViewCell: UITableViewCell {
	
	// MARK: - Properties
	@IBOutlet weak private var favouriteImageView: UIImageView!
	@IBOutlet weak private var beerImageView: UIImageView!
	@IBOutlet weak private var nameLabel: UILabel!
	@IBOutlet weak private var abvLabel: UILabel!
	@IBOutlet weak private var firstBrewedLabel: UILabel!
	@IBOutlet weak private var descriptionLabel: UILabel!
	
	// MARK: - Methods
	public func setup(beerViewModel: BeerViewModel) {
		nameLabel.text = beerViewModel.name
		abvLabel.text = "abv \(beerViewModel.abv)"
		firstBrewedLabel.text = "• \(beerViewModel.firstBrewed)"
		descriptionLabel.text = beerViewModel.description
		beerImageView.sd_setImage(with: URL(string: beerViewModel.imageURLString))
	}
}

// MARK: - NibLoadableView
extension BeerFeedTableViewCell: NibLoadableView {}

// MARK: - ReusableCell
extension BeerFeedTableViewCell: ReusableCell {}
