//
//  MaltTableViewCell.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

class MaltTableViewCell: UITableViewCell {
	
	// MARK: - Properties
	@IBOutlet weak private var name: UILabel!
	@IBOutlet weak private var amount: UILabel!
	
	// MARK: - Methods
	public func setup(maltViewModel: MaltViewModel) {
		name.text = maltViewModel.name
		amount.text = maltViewModel.amount
	}
}

// MARK: - NibLoadableView
extension MaltTableViewCell: NibLoadableView {}

// MARK: - ReusableCell
extension MaltTableViewCell: ReusableCell {}
