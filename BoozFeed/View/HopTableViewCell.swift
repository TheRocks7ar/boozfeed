//
//  HopTableViewCell.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

class HopTableViewCell: UITableViewCell {
	
	// MARK: - Properties
	@IBOutlet weak private var nameLabel: UILabel!
	@IBOutlet weak private var infoLabel: UILabel!
	@IBOutlet weak private var amountLabel: UILabel!
	
	// MARK: - Methods
	public func setup(hopViewModel: HopViewModel) {
		nameLabel.text = hopViewModel.name
		infoLabel.text = "\(hopViewModel.attribute.rawValue) • add at \(hopViewModel.add.rawValue)"
		amountLabel.text = hopViewModel.amount
	}
}

extension HopTableViewCell: NibLoadableView {}
extension HopTableViewCell: ReusableCell {}
