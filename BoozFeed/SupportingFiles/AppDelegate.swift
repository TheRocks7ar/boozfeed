//
//  AppDelegate.swift
//  BoozFeed
//
//  Created by Sayeem Hussain on 10/28/19.
//  Copyright © 2019 sayeem. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
	
  var window: UIWindow?
  let injectionContainer = BoozFeedAppDependencyContainer()
	
  func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
		let mainVC = injectionContainer.makeBeerFeedViewController()
    window = UIWindow(frame: UIScreen.main.bounds)
    window?.makeKeyAndVisible()
    window?.rootViewController = UINavigationController(rootViewController: mainVC)
		UINavigationBar.styleGlobalAppearance()
		
    return true
  }
}
